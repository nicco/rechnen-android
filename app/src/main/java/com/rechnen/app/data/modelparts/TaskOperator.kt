/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

enum class TaskOperator {
    Addition,
    Subtraction,
    Multiplication
}

object TaskOperatorUtil {
    fun getSymbol(operator: TaskOperator) = when (operator) {
        TaskOperator.Addition -> "+"
        TaskOperator.Subtraction -> "-"
        TaskOperator.Multiplication -> "*"
    }

    fun getOperator(symbol: String) = when (symbol) {
        "+" -> TaskOperator.Addition
        "-" -> TaskOperator.Subtraction
        "*" -> TaskOperator.Multiplication
        else -> throw IllegalArgumentException()
    }

    fun applySymbol(left: Int, operator: TaskOperator, right: Int) = when (operator) {
        TaskOperator.Addition -> left + right
        TaskOperator.Subtraction -> left - right
        TaskOperator.Multiplication -> left * right
    }
}