/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class Task(val left: Int, val operator: TaskOperator, val right: Int) {
    companion object {
        private const val LEFT = "left"
        private const val OPERATOR = "operator"
        private const val RIGHT = "right"

        fun parse(reader: JsonReader): Task {
            var left: Int? = null
            var operator: TaskOperator? = null
            var right: Int? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    LEFT -> left = reader.nextInt()
                    OPERATOR -> operator = TaskOperatorUtil.getOperator(reader.nextString())
                    RIGHT -> right = reader.nextInt()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return Task(
                    left = left!!,
                    operator = operator!!,
                    right = right!!
            )
        }
    }

    val result = TaskOperatorUtil.applySymbol(left, operator, right)

    override fun toString() = left.toString() + TaskOperatorUtil.getSymbol(operator) + right.toString()

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(LEFT).value(left)
        writer.name(OPERATOR).value(TaskOperatorUtil.getSymbol(operator))
        writer.name(RIGHT).value(right)

        writer.endObject()
    }
}