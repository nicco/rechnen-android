/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter
import com.rechnen.app.extension.nextIntInRange
import com.rechnen.app.extension.nextIntWithDigits
import com.rechnen.app.extension.random
import com.rechnen.app.extension.randomItem

data class TaskDifficulty(
        val roundConfig: RoundConfig = RoundConfig(),
        val multiplication: MultiplicationConfig = MultiplicationConfig(),
        val addition: AdditionConfig = AdditionConfig(),
        val subtraction: SubtractionConfig = SubtractionConfig()
) {
    companion object {
        private const val ROUND_CONFIG = "roundConfig"
        private const val MULTIPLICATION = "multiplication"
        private const val ADDITION = "addition"
        private const val SUBTRACTION = "subtraction"

        fun parse(reader: JsonReader): TaskDifficulty {
            var roundConfig: RoundConfig? = null
            var multiplication: MultiplicationConfig? = null
            var addition: AdditionConfig? = null
            var subtraction: SubtractionConfig? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ROUND_CONFIG -> roundConfig = RoundConfig.parse(reader)
                    MULTIPLICATION -> multiplication = MultiplicationConfig.parse(reader)
                    ADDITION -> addition = AdditionConfig.parse(reader)
                    SUBTRACTION -> subtraction = SubtractionConfig.parse(reader)
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return TaskDifficulty(
                    roundConfig = roundConfig!!,
                    multiplication = multiplication!!,
                    addition = addition!!,
                    subtraction = subtraction!!
            )
        }
    }

    private val enabledOperators: List<TaskOperator> by lazy {
        val result = mutableListOf<TaskOperator>()

        if (multiplication.enable) { result.add(TaskOperator.Multiplication) }
        if (addition.enable) { result.add(TaskOperator.Addition) }
        if (subtraction.enable) { result.add(TaskOperator.Subtraction) }

        result
    }

    fun generateTask() = generateTask(enabledOperators.randomItem(random))

    private fun generateTask(operator: TaskOperator): Task = when (operator) {
        TaskOperator.Multiplication -> Task(
                left = random.nextIntWithDigits(multiplication.digitsOfFirstNumber),
                right = random.nextIntWithDigits(multiplication.digitsOfSecondNumber),
                operator = TaskOperator.Multiplication
        )
        TaskOperator.Addition -> {
            val left = random.nextIntWithDigits(addition.digitsOfFirstNumber)

            val right = if (addition.enableTenTransgression) {
                random.nextIntWithDigits(addition.digitsOfSecondNumber)
            } else {
                random.nextIntInRange(1, 10 - left % 10)
            }

            Task(
                    left = left,
                    right = right,
                    operator = TaskOperator.Addition
            )
        }
        TaskOperator.Subtraction -> {
            val left = random.nextIntWithDigits(subtraction.digitsOfFirstNumber)

            val right = if (!subtraction.enableTenTransgression) {
                random.nextIntInRange(1, left % 10)
            } else {
                random.nextIntWithDigits(subtraction.digitsOfSecondNumber)
            }

            if (left < right && !subtraction.allowNegativeResults)
                Task(
                        left = right,
                        right = left,
                        operator = TaskOperator.Subtraction
                )
            else
                Task(
                        left = left,
                        right = right,
                        operator = TaskOperator.Subtraction
                )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ROUND_CONFIG); roundConfig.serialize(writer)
        writer.name(MULTIPLICATION); multiplication.serialize(writer)
        writer.name(ADDITION); addition.serialize(writer)
        writer.name(SUBTRACTION); subtraction.serialize(writer)

        writer.endObject()
    }
}