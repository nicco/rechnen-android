/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rechnen.app.data.modelparts.TaskDifficulty
import com.rechnen.app.data.model.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun getAllUsers(): LiveData<List<User>>

    @Query("SELECT * FROM user WHERE id = :userId")
    suspend fun getUserByIdCoroutine(userId: Int): User?

    @Insert
    fun addUserSync(user: User)

    @Query("DELETE FROM user WHERE id = :userId")
    fun deleteUserByIdSync(userId: Int)

    @Query("UPDATE user SET last_use = :timestamp, last_block_count = :blockCount WHERE id = :userId")
    suspend fun updateUserLastUseAndBlockCountCoroutine(userId: Int, timestamp: Long, blockCount: Int)

    @Query("UPDATE user SET difficulty = :difficulty WHERE id = :userId")
    fun updateUserDifficultyConfigSync(userId: Int, difficulty: TaskDifficulty)
}