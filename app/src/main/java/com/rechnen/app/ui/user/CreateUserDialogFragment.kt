/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.rechnen.app.R
import com.rechnen.app.data.modelparts.TaskDifficultyProfiles
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.data.model.User
import com.rechnen.app.databinding.CreateUserDialogBinding

class CreateUserDialogFragment: DialogFragment() {
    companion object {
        private const val DIALOG_TAG = "CreateUserDialogFragment"
    }

    private lateinit var binding: CreateUserDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?) = BottomSheetDialog(context!!, theme).apply {
        setOnShowListener {
            Threads.mainThreadHandler.post {
                binding.username.requestFocus()
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .showSoftInput(binding.username, 0)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CreateUserDialogBinding.inflate(inflater, container, false)

        binding.createBtn.setOnClickListener { handleCreateUser() }

        binding.username.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                handleCreateUser()

                true
            } else {
                false
            }
        }

        binding.username.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                handleCreateUser()

                true
            } else {
                false
            }
        }


        return binding.root
    }

    private fun handleCreateUser() {
        val name = binding.username.text.toString()
        val database = AppDatabase.with(context!!)

        if (name.isBlank()) {
            Toast.makeText(context, R.string.user_list_create_user_no_name_error, Toast.LENGTH_SHORT).show()
            return
        }


        Threads.database.submit {
            database.user().addUserSync(
                    User(
                            id = 0,
                            name = name,
                            difficulty = TaskDifficultyProfiles.medium,
                            lastBlockCount = 0,
                            lastUse = null
                    )
            )
        }

        Toast.makeText(context, getString(R.string.user_list_created_user, name), Toast.LENGTH_SHORT).show()

        dismiss()
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}