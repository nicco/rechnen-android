/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.rechnen.app.R
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase

class DeleteUserDialogFragment: DialogFragment() {
    companion object {
        private const val DIALOG_TAG = "DeleteUserDialogFragment"
        private const val ARG_USER_ID = "userId"
        private const val ARG_USER_TItLE = "userTitle"

        fun newInstance(userId: Int, userTitle: String) = DeleteUserDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_USER_ID, userId)
                putString(ARG_USER_TItLE, userTitle)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val userId = arguments!!.getInt(ARG_USER_ID)
        val userTitle = arguments!!.getString(ARG_USER_TItLE)

        return AlertDialog.Builder(context!!, theme)
                .setMessage(getString(R.string.user_list_delete_user_question, userTitle))
                .setNegativeButton(R.string.generic_cancel, null)
                .setPositiveButton(R.string.user_list_delete_user) { _, _ ->
                    val database = AppDatabase.with(context!!)

                    Threads.database.execute {
                        database.user().deleteUserByIdSync(userId)
                    }

                    Toast.makeText(context, R.string.user_list_deleted_user, Toast.LENGTH_SHORT).show()
                }
                .create()
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}