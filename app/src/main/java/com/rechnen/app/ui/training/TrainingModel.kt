/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rechnen.app.data.Database
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

class TrainingModel: ViewModel() {
    private val input = Channel<TrainingInputEvent>(Channel.CONFLATED)
    private val job = Job()
    private val scope = GlobalScope + job
    private val stateInternal = MutableLiveData<TrainingState>()
    private val currentNumberInputInternal = MutableLiveData<String>()
    private var didInit = false

    val state: LiveData<TrainingState> = stateInternal
    val currentNumberInput: LiveData<String> = currentNumberInputInternal

    fun init(
            database: Database,
            userId: Int
    ) {
        if (didInit) {
            return
        }

        didInit = true

        scope.launch {
            TrainingUtil.start(
                    input = input,
                    database = database,
                    userId = userId,
                    scope = scope
            ).consumeEach {
                stateInternal.postValue(it)

                if (it is ShowTaskTrainingState && it.state == TrainingShowTaskMode.WaitingForInput) {
                    currentNumberInputInternal.postValue("")
                }
            }
        }
    }

    fun handleNumberButton(digit: Int) {
        val oldValue = currentNumberInput.value ?: ""

        currentNumberInputInternal.value = oldValue + digit.toString()
    }

    fun handleMinusButton() {
        val oldValue = currentNumberInput.value ?: ""

        currentNumberInputInternal.value = if (oldValue.startsWith("-")) oldValue.substring(1) else "-" + oldValue
    }

    fun handleDeleteButton() {
        val oldValue = currentNumberInput.value

        if (oldValue != null && oldValue.isNotEmpty()) {
            currentNumberInputInternal.value = oldValue.substring(0, oldValue.length - 1)
        }
    }

    fun handleOkButton() {
        val value = currentNumberInput.value?.toIntOrNull() ?: 0

        input.offer(ResultTrainingInputEvent(value))
    }

    fun handleContinueButton() {
        input.offer(ContinueTrainingInputEvent)
    }

    override fun onCleared() {
        super.onCleared()

        job.cancel()
    }
}