/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.os.SystemClock
import com.rechnen.app.data.modelparts.Task
import com.rechnen.app.data.Database
import com.rechnen.app.data.model.SavedTask
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object TrainingUtil {
    private fun now() = SystemClock.elapsedRealtime()

    fun start(
            input: ReceiveChannel<TrainingInputEvent>,
            database: Database,
            userId: Int,
            scope: CoroutineScope
    ): ReceiveChannel<TrainingState> = scope.produce {
        var blockCount = 0

        input.consume {
            val user = database.user().getUserByIdCoroutine(userId)!!

            while (true) {
                // do round
                database.user().updateUserLastUseAndBlockCountCoroutine(
                        userId = userId,
                        timestamp = System.currentTimeMillis(),
                        blockCount = ++blockCount
                )

                val roundStartTime = now()
                var delayedTime = 0L

                val savedTasks = database.savedTask().getUserTasksCoroutine(userId).map { it.task }

                val tasks = if (savedTasks.isEmpty())
                        (1..user.difficulty.roundConfig.tasksPerRound).map { user.difficulty.generateTask() }
                else
                    savedTasks

                if (tasks.size != savedTasks.size) {
                    withContext(Dispatchers.IO) {
                        database.runInTransaction {
                            database.savedTask().deleteUserTasksSync(userId)

                            database.savedTask().addUserTasksSync(
                                    tasks.map {
                                        SavedTask(
                                                id = 0,
                                                userId = userId,
                                                task = it
                                        )
                                    }
                            )
                        }
                    }
                }

                val rightTasks = mutableListOf<Task>()
                val wrongTasks = mutableListOf<Task>()

                tasks.shuffled().forEachIndexed { index, task ->
                    val taskObject = ShowTaskTrainingState(task, index, tasks.size, TrainingShowTaskMode.WaitingForInput)

                    send(taskObject)

                    lateinit var answer: ResultTrainingInputEvent
                    while (true) {
                        val value = input.receive()

                        if (value is ResultTrainingInputEvent) {
                            answer = value
                            break
                        }
                    }

                    val valid = answer.result == task.result

                    if (valid) {
                        rightTasks.add(task)
                        send(taskObject.copy(state = TrainingShowTaskMode.FeedbackRight))

                        delay(500); delayedTime += 500
                    } else {
                        wrongTasks.add(task)
                        send(taskObject.copy(state = TrainingShowTaskMode.FeedbackWrong))

                        delay(1000); delayedTime += 1000
                    }
                }

                val roundEndTime = now()
                val roundDuration = roundEndTime - roundStartTime - delayedTime

                val mistakes = wrongTasks.size
                val mistakeCountOk = mistakes <= user.difficulty.roundConfig.maximalMistakesPerRound
                val timeOk = roundDuration <= user.difficulty.roundConfig.maximalTimePerRound
                val userGetsNewTasks = mistakeCountOk && timeOk

                val result = if (mistakeCountOk)
                    if (timeOk)
                        TrainingRoundEndState.EverythingGood
                    else
                        TrainingRoundEndState.TooSlow
                else
                    TrainingRoundEndState.TooMuchMistakes

                if (userGetsNewTasks) {
                    val nextTasks = mutableListOf<Task>()

                    rightTasks.shuffle()
                    wrongTasks.shuffle()

                    if (rightTasks.isNotEmpty()) {
                        nextTasks.addAll(
                                rightTasks.subList(0, user.difficulty.roundConfig.reAskRightInNextRound.coerceAtMost(rightTasks.size - 1))
                        )
                    }

                    if (wrongTasks.isNotEmpty()) {
                        nextTasks.addAll(
                                wrongTasks.subList(0, user.difficulty.roundConfig.reAskWrongInNextRound.coerceAtMost(wrongTasks.size - 1))
                        )
                    }

                    while (nextTasks.size < user.difficulty.roundConfig.tasksPerRound) {
                        nextTasks.add(user.difficulty.generateTask())
                    }

                    nextTasks.shuffle()

                    withContext(Dispatchers.IO) {
                        database.runInTransaction {
                            database.savedTask().deleteUserTasksSync(userId)

                            database.savedTask().addUserTasksSync(
                                    nextTasks.map {
                                        SavedTask(
                                                id = 0,
                                                userId = userId,
                                                task = it
                                        )
                                    }
                            )
                        }
                    }
                }

                send(
                        EndOfRoundState(
                                mistakes = mistakes,
                                time = roundDuration,
                                result = result
                        )
                )

                while (true) {
                    val value = input.receive()

                    if (value is ContinueTrainingInputEvent) {
                        break
                    }
                }
            }
        }
    }
}

sealed class TrainingInputEvent
data class ResultTrainingInputEvent(val result: Int): TrainingInputEvent()
object ContinueTrainingInputEvent: TrainingInputEvent()

sealed class TrainingState
data class ShowTaskTrainingState(val task: Task, val currentQuestionIndex: Int, val totalQuestionCounter: Int, val state: TrainingShowTaskMode): TrainingState()
data class EndOfRoundState(val mistakes: Int, val time: Long, val result: TrainingRoundEndState): TrainingState()

enum class TrainingRoundEndState {
    EverythingGood,
    TooSlow,
    TooMuchMistakes
}

enum class TrainingShowTaskMode {
    WaitingForInput,
    FeedbackWrong,
    FeedbackRight
}