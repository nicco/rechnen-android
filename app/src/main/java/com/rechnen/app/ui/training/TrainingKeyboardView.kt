/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import com.rechnen.app.databinding.TrainingKeyboardViewBinding

object TrainingKeyboardView {
    fun setListener(view: TrainingKeyboardViewBinding, listener: TrainingKeyboardViewListener) {
        view.okButton.setOnClickListener { listener.onInputConfirmed() }
        view.minusButton.setOnClickListener { listener.onMinusClicked() }

        listOf(
                view.button0,
                view.button1,
                view.button2,
                view.button3,
                view.button4,
                view.button5,
                view.button6,
                view.button7,
                view.button8,
                view.button9
        ).forEachIndexed { index, button ->
            button.setOnClickListener { listener.onDigitEntered(index) }
        }
    }
}

interface TrainingKeyboardViewListener {
    fun onDigitEntered(digit: Int)
    fun onMinusClicked()
    fun onInputConfirmed()
}