/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rechnen.app.R
import com.rechnen.app.data.modelparts.TaskDifficulty
import com.rechnen.app.data.modelparts.TaskDifficultyProfiles
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.databinding.DifficultyDialogBinding
import com.rechnen.app.extension.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EditUserDifficultyDialog: BottomSheetDialogFragment() {
    companion object {
        private const val DIALOG_TAG = "EditUserDifficultyDialog"
        private const val EXTRA_USER_ID = "userId"
        private const val STATE_DIFFICULTY = "difficulty"
        private const val STATE_SCREEN = "screen"

        private const val PAGE_OVERVIEW = 0
        private const val PAGE_MULTIPLICATION = 1
        private const val PAGE_ADDITION = 2
        private const val PAGE_SUBTRACTION = 3
        private const val PAGE_PROFILES = 4
        private const val PAGE_LOADING = 5
        private const val PAGE_GENERAL = 6

        fun newInstance(userId: Int) = EditUserDifficultyDialog().apply {
            arguments = Bundle().apply {
                putInt(EXTRA_USER_ID, userId)
            }
        }
    }

    private var didLoadDifficulty = false
    private var isUpdatingValues = false
    private lateinit var difficulty: TaskDifficulty
    private lateinit var binding: DifficultyDialogBinding

    private fun formatDigitNumber(n: Int) = resources.getQuantityString(R.plurals.difficulty_digit_counter, n, n)

    private fun updateOverview() {
        val withTenTransgression = getString(R.string.suffix_with_ten_transgression)
        val withoutTenTransgression = getString(R.string.suffix_without_ten_transgression)

        binding.difficultyStatus = getString(
                TaskDifficultyProfiles.profilesAndLabels.find { it.first == difficulty }?.second
                        ?: R.string.difficulty_profile_custom
        )

        binding.additionStatus = if (difficulty.addition.enable) {
            getString(
                    R.string.difficulty_operation_addition_enabled,
                    formatDigitNumber(difficulty.addition.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.addition.digitsOfSecondNumber),
                    if (difficulty.addition.digitsOfSecondNumber > 1)
                        ""
                    else if (difficulty.addition.enableTenTransgression)
                        withTenTransgression
                    else
                        withoutTenTransgression
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.subtractionStatus = if (difficulty.subtraction.enable) {
            getString(
                    R.string.difficulty_operation_subtraction_enabled,
                    formatDigitNumber(difficulty.subtraction.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.subtraction.digitsOfSecondNumber),
                    if (difficulty.subtraction.allowNegativeResults)
                        getString(R.string.suffix_with_negative_results)
                    else if (difficulty.subtraction.digitsOfSecondNumber > 1)
                        ""
                    else if (difficulty.subtraction.enableTenTransgression)
                        withTenTransgression
                    else
                        withoutTenTransgression
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.multiplicationStatus = if (difficulty.multiplication.enable) {
            getString(
                    R.string.difficulty_operation_multiplication_enabled,
                    formatDigitNumber(difficulty.multiplication.digitsOfFirstNumber),
                    formatDigitNumber(difficulty.multiplication.digitsOfSecondNumber)
            )
        } else {
            getString(R.string.difficulty_operation_disabled)
        }

        binding.generalStatus = getString(
                R.string.difficulty_general_summary,
                resources.getQuantityString(R.plurals.difficulty_general_summary_tasks, difficulty.roundConfig.tasksPerRound, difficulty.roundConfig.tasksPerRound),
                resources.getQuantityString(
                        R.plurals.training_result_seconds,
                        difficulty.roundConfig.maximalTimePerRound / 1000,
                        difficulty.roundConfig.maximalTimePerRound / 1000
                ),
                resources.getQuantityString(R.plurals.difficulty_general_summary_mistake, difficulty.roundConfig.maximalMistakesPerRound, difficulty.roundConfig.maximalMistakesPerRound),
                difficulty.roundConfig.reAskWrongInNextRound,
                difficulty.roundConfig.reAskRightInNextRound
        )
    }

    private fun updateMultiplication() {
        val allowDisabling = difficulty.addition.enable || difficulty.subtraction.enable

        val binding = binding.multiplication
        val difficulty = difficulty.multiplication

        binding.enableOperation = difficulty.enable
        binding.allowDisabling = allowDisabling

        isUpdatingValues = true
        binding.enable.isChecked = difficulty.enable
        binding.digits1.progress = difficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = difficulty.digitsOfSecondNumber - 1
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(difficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(difficulty.digitsOfSecondNumber)
    }

    private fun updateAddition() {
        val allowDisabling = difficulty.multiplication.enable || difficulty.subtraction.enable

        val binding = binding.addition
        val difficulty = difficulty.addition

        binding.enableOperation = difficulty.enable
        binding.allowDisabling = allowDisabling

        isUpdatingValues = true
        binding.enable.isChecked = difficulty.enable
        binding.digits1.progress = difficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = difficulty.digitsOfSecondNumber - 1
        binding.allowAdjustSecondOperandSize = difficulty.enableTenTransgression
        binding.tenTransgressionSwitch.isChecked = difficulty.enableTenTransgression
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(difficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(difficulty.digitsOfSecondNumber)
    }

    private fun updateSubtraction() {
        val allowDisabling = difficulty.addition.enable || difficulty.multiplication.enable

        val binding = binding.subtraction
        val difficulty = difficulty.subtraction

        binding.enableOperation = difficulty.enable
        binding.allowDisabling = allowDisabling

        isUpdatingValues = true
        binding.enable.isChecked = difficulty.enable
        binding.digits1.progress = difficulty.digitsOfFirstNumber - 1
        binding.digits2.progress = difficulty.digitsOfSecondNumber - 1
        binding.tenTransgressionSwitch.isChecked = difficulty.enableTenTransgression
        binding.negativeResultSwitch.isChecked = difficulty.allowNegativeResults
        binding.allowAdjustSecondOperandSize = difficulty.enableTenTransgression
        binding.allowAdjustNegativeResults = difficulty.enableTenTransgression
        isUpdatingValues = false

        binding.firstDigitsText = formatDigitNumber(difficulty.digitsOfFirstNumber)
        binding.secondDigitsText = formatDigitNumber(difficulty.digitsOfSecondNumber)
    }

    private fun updateProfiles() {
        binding.profile.easy.isChecked = difficulty == TaskDifficultyProfiles.easy
        binding.profile.medium.isChecked = difficulty == TaskDifficultyProfiles.medium
        binding.profile.hard.isChecked = difficulty == TaskDifficultyProfiles.hard
    }

    private fun updateGeneral() {
        val binding = binding.general
        val config = difficulty.roundConfig

        binding.tasksPerRoundText = resources.getQuantityString(R.plurals.difficulty_tasks_per_block, config.tasksPerRound, config.tasksPerRound)
        binding.timePerRoundText = getString(
                R.string.difficulty_time_per_round,
                resources.getQuantityString(R.plurals.training_result_seconds, config.maximalTimePerRound / 1000, config.maximalTimePerRound / 1000)
        )
        binding.maxWrongTasksText = resources.getQuantityString(R.plurals.difficulty_max_wrong_tasks, config.maximalMistakesPerRound, config.maximalMistakesPerRound)
        binding.takeRightTasksText = resources.getQuantityString(R.plurals.difficulty_take_right_tasks, config.reAskRightInNextRound, config.reAskRightInNextRound)
        binding.takeWrongTasksText = resources.getQuantityString(R.plurals.difficulty_take_wrong_tasks, config.reAskWrongInNextRound, config.reAskWrongInNextRound)

        isUpdatingValues = true

        binding.timePerRound.progress = config.maximalTimePerRound / (config.tasksPerRound * 1000) - 1
        binding.tasksPerRound.progress = config.tasksPerRound / 5 - 1

        binding.maxWrongTasks.max = config.tasksPerRound
        binding.maxWrongTasks.progress = config.maximalMistakesPerRound

        binding.takeWrongTasks.max = config.maximalMistakesPerRound
        binding.takeWrongTasks.progress = config.reAskWrongInNextRound

        binding.takeRightTasks.max = config.tasksPerRound
        binding.takeRightTasks.progress = config.reAskRightInNextRound

        isUpdatingValues = false
    }

    private fun updateAll() {
        updateOverview()
        updateMultiplication()
        updateAddition()
        updateSubtraction()
        updateProfiles()
        updateGeneral()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val database = AppDatabase.with(context!!)
        val userId = arguments!!.getInt(EXTRA_USER_ID)

        binding = DifficultyDialogBinding.inflate(inflater, container, false)

        val oldDifficulty: TaskDifficulty? = savedInstanceState?.getString(STATE_DIFFICULTY)?.useJsonReader { TaskDifficulty.parse(it) }

        if (oldDifficulty != null) {
            difficulty = oldDifficulty
            didLoadDifficulty = true

            updateAll()

            binding.flipper.displayedChild = savedInstanceState.getInt(STATE_SCREEN, PAGE_OVERVIEW)
        } else {
            binding.flipper.displayedChild = PAGE_LOADING

            GlobalScope.launch (Dispatchers.Main) {
                difficulty = database.user().getUserByIdCoroutine(userId)!!.difficulty
                didLoadDifficulty = true

                updateAll()
                binding.flipper.displayedChild = PAGE_OVERVIEW
            }
        }

        binding.profileButton.setOnClickListener { updateProfiles(); binding.flipper.openSubscreen(PAGE_PROFILES) }
        binding.multiplicationButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_MULTIPLICATION) }
        binding.additonButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_ADDITION) }
        binding.subtractionButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_SUBTRACTION) }
        binding.generalButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_GENERAL) }
        binding.saveButton.setOnClickListener {
            Threads.database.submit {
                database.runInTransaction {
                    database.savedTask().deleteUserTasksSync(userId)
                    database.user().updateUserDifficultyConfigSync(userId, difficulty)
                }
            }

            Toast.makeText(context!!, R.string.difficulty_save_toast, Toast.LENGTH_SHORT).show()

            dismiss()
        }

        binding.multiplication.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_multiplication)
            binding.allowAdjustSecondOperandSize = true

            binding.enable.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.visibility = View.GONE
            binding.negativeResultSwitch.visibility = View.GONE

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    digitsOfFirstNumber = it + 1
                            )
                    )

                    updateMultiplication()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            multiplication = difficulty.multiplication.copy(
                                    digitsOfSecondNumber = it + 1
                            )
                    )

                    updateMultiplication()
                }
            }
        }

        binding.addition.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_addition)

            binding.enable.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    enableTenTransgression = enable,
                                    digitsOfSecondNumber = if (enable) difficulty.addition.digitsOfSecondNumber else 1
                            )
                    )

                    updateAddition()
                }
            }

            binding.negativeResultSwitch.visibility = View.GONE

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    digitsOfFirstNumber = it + 1
                            )
                    )

                    updateAddition()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            addition = difficulty.addition.copy(
                                    digitsOfSecondNumber = it + 1
                            )
                    )

                    updateAddition()
                }
            }
        }

        binding.subtraction.let { binding ->
            binding.operationTitle = getString(R.string.difficulty_operation_subtraction)

            binding.enable.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    enable = enable
                            )
                    )

                    updateAll() // because it can change if an other operation can be disabled
                }
            }

            binding.tenTransgressionSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    enableTenTransgression = enable,
                                    digitsOfSecondNumber = if (enable) difficulty.subtraction.digitsOfSecondNumber else 1,
                                    allowNegativeResults = enable && difficulty.subtraction.allowNegativeResults
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.negativeResultSwitch.setOnCheckedChangeListener { _, enable ->
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    allowNegativeResults = enable,
                                    digitsOfSecondNumber = if (enable)
                                        difficulty.subtraction.digitsOfSecondNumber
                                    else
                                        difficulty.subtraction.digitsOfSecondNumber.coerceAtMost(difficulty.subtraction.digitsOfFirstNumber)
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.digits1.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    digitsOfFirstNumber = it + 1,
                                    allowNegativeResults = difficulty.subtraction.allowNegativeResults || difficulty.subtraction.digitsOfSecondNumber > it + 1
                            )
                    )

                    updateSubtraction()
                }
            }

            binding.digits2.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            subtraction = difficulty.subtraction.copy(
                                    digitsOfSecondNumber = it + 1,
                                    allowNegativeResults = difficulty.subtraction.allowNegativeResults || it + 1 > difficulty.subtraction.digitsOfFirstNumber
                            )
                    )

                    updateSubtraction()
                }
            }
        }

        binding.profile.apply {
            easy.setOnClickListener {
                difficulty = TaskDifficultyProfiles.easy

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }

            medium.setOnClickListener {
                difficulty = TaskDifficultyProfiles.medium

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }

            hard.setOnClickListener {
                difficulty = TaskDifficultyProfiles.hard

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
                updateAll()
            }
        }

        binding.general.let { binding ->
            binding.tasksPerRound.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = (it + 1) * 5

                    difficulty = difficulty.copy(
                            roundConfig = difficulty.roundConfig.copy(
                                    tasksPerRound = new,
                                    maximalTimePerRound = (old.maximalTimePerRound * ((it + 1) * 5 ) / old.tasksPerRound),
                                    maximalMistakesPerRound = old.maximalMistakesPerRound.coerceAtMost(new),
                                    reAskRightInNextRound = old.reAskRightInNextRound.coerceAtMost(new),
                                    reAskWrongInNextRound = old.reAskWrongInNextRound.coerceAtMost(new)
                            )
                    )

                    updateGeneral()
                }
            }

            binding.timePerRound.setOnChangeListener {
                if (!isUpdatingValues) {
                    difficulty = difficulty.copy(
                            roundConfig = difficulty.roundConfig.copy(
                                    maximalTimePerRound = (it + 1) * (difficulty.roundConfig.tasksPerRound * 1000)
                            )
                    )

                    updateGeneral()
                }
            }

            binding.takeRightTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    reAskRightInNextRound = new
                            )
                    )

                    updateGeneral()
                }
            }

            binding.takeWrongTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    reAskWrongInNextRound = new
                            )
                    )

                    updateGeneral()
                }
            }

            binding.maxWrongTasks.setOnChangeListener {
                if (!isUpdatingValues) {
                    val old = difficulty.roundConfig
                    val new = it

                    difficulty = difficulty.copy(
                            roundConfig = old.copy(
                                    maximalMistakesPerRound = new,
                                    reAskWrongInNextRound = old.reAskWrongInNextRound.coerceAtMost(new)
                            )
                    )

                    updateGeneral()
                }
            }
        }

        // enabled until the view state was restored => see below
        isUpdatingValues = true

        return binding.root
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        isUpdatingValues = false

        // eventually restore values which were modified by the view restoration
        if (didLoadDifficulty) {
            updateAll()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (didLoadDifficulty) {
            outState.putString(STATE_DIFFICULTY, writeJson { difficulty.serialize(it) })
            outState.putInt(STATE_SCREEN, binding.flipper.displayedChild)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = object: BottomSheetDialog(context!!, theme) {
        override fun onBackPressed() {
            if (binding.flipper.displayedChild != PAGE_OVERVIEW && binding.flipper.displayedChild != PAGE_LOADING) {
                updateOverview()

                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            } else {
                super.onBackPressed()
            }
        }
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}