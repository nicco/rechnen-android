/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.rechnen.app.R
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.databinding.ActivityTrainingMainBinding

class TrainingMainActivity : FragmentActivity() {
    companion object {
        private const val EXTRA_USER_ID = "userId"

        fun launch(context: Context, userId: Int) {
            context.startActivity(
                    Intent(context, TrainingMainActivity::class.java)
                            .putExtra(EXTRA_USER_ID, userId)
            )
        }
    }

    private val model: TrainingModel by lazy {
        ViewModelProviders.of(this).get(TrainingModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityTrainingMainBinding>(this, R.layout.activity_training_main)
        val userId = intent.extras!!.getInt(EXTRA_USER_ID)

        model.init(
                database = AppDatabase.with(this),
                userId = userId
        )

        model.state.observe(this, Observer { state ->
            when (state) {
                is ShowTaskTrainingState -> {
                    binding.flipper.displayedChild = 0

                    binding.work.text = "${state.task}="

                    binding.progress.apply {
                        progress = state.currentQuestionIndex + 1
                        max = state.totalQuestionCounter
                    }

                    binding.keyboard.isEnabled = state.state == TrainingShowTaskMode.WaitingForInput
                    binding.inputLineEndFlipper.displayedChild = when (state.state) {
                        TrainingShowTaskMode.WaitingForInput -> 0
                        TrainingShowTaskMode.FeedbackRight -> 1
                        TrainingShowTaskMode.FeedbackWrong -> 2
                    }

                    null
                }
                is EndOfRoundState -> {
                    binding.flipper.displayedChild = 1

                    binding.resultScreen.wasGood = state.result == TrainingRoundEndState.EverythingGood
                    binding.resultScreen.resultText = when (state.result) {
                        TrainingRoundEndState.EverythingGood -> if (state.mistakes == 0)
                            getString(R.string.training_block_result_perfect)
                        else
                            resources.getQuantityString(R.plurals.training_block_result_good, state.mistakes, state.mistakes)
                        TrainingRoundEndState.TooMuchMistakes -> resources.getQuantityString(R.plurals.training_block_result_too_much_mistakes, state.mistakes, state.mistakes)
                        TrainingRoundEndState.TooSlow -> resources.getString(R.string.training_block_result_too_slow, formatMistakes(state.mistakes), formatSeconds((state.time / 1000).toInt()))
                    }
                }
            }.let {/* require handling all cases */}
        })

        model.currentNumberInput.observe(this, Observer { binding.result.text = it })

        TrainingKeyboardView.setListener(binding.keyboard, object: TrainingKeyboardViewListener {
            override fun onDigitEntered(digit: Int) = model.handleNumberButton(digit)
            override fun onMinusClicked() = model.handleMinusButton()
            override fun onInputConfirmed() = model.handleOkButton()
        })

        binding.deleteButton.setOnClickListener { model.handleDeleteButton() }

        binding.resultScreen.endButton.setOnClickListener { finish() }
        binding.resultScreen.continueButton.setOnClickListener { model.handleContinueButton() }
    }

    private fun formatMistakes(mistakes: Int) = resources.getQuantityString(R.plurals.training_result_mistakes, mistakes, mistakes)
    private fun formatSeconds(seconds: Int) = resources.getQuantityString(R.plurals.training_result_seconds, seconds, seconds)
}
